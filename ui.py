from PyQt6.QtWidgets import (QWidget, QSlider, QLineEdit, QLabel, QPushButton, QScrollArea,QApplication,
                             QHBoxLayout, QVBoxLayout, QMainWindow, QComboBox, QMessageBox)
from PyQt6.QtCore import Qt, QSize, QAbstractTableModel
from PyQt6 import QtWidgets, uic
import sys
import random
import time
import paho.mqtt.client as paho
from queue import Queue

topicosArray  = []
filaMensagens = []

queueMessages = Queue()

class Client():
    def __init__(self, main):
        super().__init__()

        self.broker = 'broker.emqx.io'
        self.port = 1883
        self.client_id = f'python-mqtt-{random.randint(0, 1000)}'
        self.mainWindow = main

        self.client = self.connectMqtt()
        self.checkUnsend()
        self.client.loop_start()
    
    def connectMqtt(self):
        def on_connect(client, userdata, flags, rc):
            client.subscribe('Geral')
            if rc == 0:
                print("Conectado ao MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        def on_message(client, userdata, msg):  # The callback for when a PUBLISH =
            payloadMsg = msg.payload.decode()
            payloadMessage = "[{}] {}".format(msg.topic, str(payloadMsg))
            queueMessages.put(payloadMessage)
            self.mainWindow.adicionaMensagens()

        client = paho.Client(self.client_id)
        client.on_connect = on_connect
        client.on_message = on_message
        # client.user_data_set()
        client.connect(self.broker, self.port)
        return client

    def disconnectMqtt(self):
        self.client.disconnect()
        self.client.loop_stop()

    def checkUnsend(self):
        if filaMensagens != []:
            for message in filaMensagens:
                self.publish(message[0], message[1])

    def subscribeTopic(self, topic):
        self.client.subscribe(topic)

    def publish(self, msg, topic):
        result = self.client.publish(topic, msg)
        # result: [0, 1]
        status = result[0]
        if status == 0:
            print(f"Send `{msg}` to topic `{topic}`")
        else:
            print(f"Failed to send message to topic {topic}")

class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.initUI()
        self.client = None

    def adicionaMensagens(self):
        #later in code
        while not queueMessages.empty():
            message = queueMessages.get()
            if message is None:
                continue
            self.vbox.addWidget(QLabel(message))

    def adicionaTopico(self, topico):
        self.client.subscribeTopic(topico)
        self.caixaSelect.addItem(topico)

    def enviarMensagem(self):
        textoDigitado = self.caixaMensagem.text()
        topicoSelecionado = self.caixaSelect.currentText()
        if textoDigitado != '':
            if self.client != None:
                self.client.publish(textoDigitado, topicoSelecionado)
            else:
                filaMensagens.append([textoDigitado, topicoSelecionado])

    def setOnOff(self):
        if self.toggleButton.isChecked(): 
            if self.client == None:
                print("oi")
                self.client = Client(self)
                self.topicWindow = TopicWindow(self)
                self.toggleButton.setText("Online")
                self.toggleButton.setStyleSheet("background-color : green; color: black")
        else:
            self.client.disconnectMqtt()
            self.client = None
            self.topicWindow.close()
            self.toggleButton.setText("Offline")
            self.toggleButton.setStyleSheet("background-color : red; color: white")

    def initUI(self):
        self.scroll = QScrollArea()             # Scroll Area which contains the widgets, set as the centralWidget
        self.widget = QWidget()                 # Widget that contains the collection of Vertical Box
        self.vbox   = QVBoxLayout()             # The Vertical Box that contains the Horizontal Boxes of  labels and buttons

        #Scroll Area Properties
        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOn)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.widget)

        self.setCentralWidget(self.scroll)

        # BOTAO ONLINE/OFFLINE
        self.toggleButton = QPushButton("Offline", self)
        self.toggleButton.setCheckable(True)
        self.toggleButton.clicked.connect(self.setOnOff)
        self.toggleButton.setStyleSheet("background-color : red; color: white")
        self.vbox.addWidget(self.toggleButton)

        self.caixaMensagem = QLineEdit("")
        self.caixaMensagem.setGeometry(100, 100, 150, 40)
        self.vbox.addWidget(self.caixaMensagem)
        
        self.caixaSelect = QComboBox()
        self.caixaSelect.setGeometry(300, 100, 150, 40)
        self.caixaSelect.addItem('Geral')
        self.vbox.addWidget(self.caixaSelect)

        btnMessage = QPushButton("Enviar")
        btnMessage.setGeometry(500, 100, 150, 40)
        btnMessage.clicked.connect(self.enviarMensagem)
        self.vbox.addWidget(btnMessage)
        
        self.widget.setLayout(self.vbox)

        # self.setGeometry(600, 100, 1000, 900)
        self.setWindowTitle('Projeto MoM')
        self.show()

        self.adicionaMensagens()

        return

class TopicWindow(QMainWindow):

    def __init__(self, main):
        super().__init__()
        self.mainWindow = main
        self.initUI()

    def criarTopico(self):
        textoDigitado = self.caixaMensagem.text()
        if textoDigitado != '' and textoDigitado not in topicosArray:
            topicosArray.append(textoDigitado)
            self.mainWindow.adicionaTopico(textoDigitado)
            self.vbox.addWidget(QLabel(textoDigitado))
        else:
            msg = QMessageBox()
            msg.setWindowTitle("Alerta")
            msg.setText("Topico ja existente!")
            x = msg.exec()

    def initUI(self):
        self.scroll = QScrollArea()             # Scroll Area which contains the widgets, set as the centralWidget
        self.widget = QWidget()                 # Widget that contains the collection of Vertical Box
        self.vbox   = QVBoxLayout()             # The Vertical Box that contains the Horizontal Boxes of  labels and buttons

        #Scroll Area Properties
        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOn)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.widget)

        self.setCentralWidget(self.scroll)

        self.caixaMensagem = QLineEdit("")
        self.vbox.addWidget(self.caixaMensagem)

        btnMessage = QPushButton("Enviar")
        btnMessage.clicked.connect(self.criarTopico)
        self.vbox.addWidget(btnMessage)
        
        self.vbox.addWidget(QLabel('Geral'))

        self.widget.setLayout(self.vbox)

        # self.setGeometry(600, 100, 1000, 900)
        self.setWindowTitle('Topicos')
        self.show()

        return

def main():
    app = QtWidgets.QApplication(sys.argv)
    main = MainWindow()
    sys.exit(app.exec())

if __name__ == '__main__':
    main()